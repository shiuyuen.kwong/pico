import threading
import sys
from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QWidget, QLineEdit, QGridLayout, QFrame
from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice

# Define the COM port and baud rate
COM_PORT = "COM13"
BAUD_RATE = 9600

class SerialReader(QObject, threading.Thread):
    data_received = pyqtSignal(str, int, int, int)

    def __init__(self, gui):
        QObject.__init__(self)
        threading.Thread.__init__(self)
        self.gui = gui
        self.stop_event = threading.Event()

    def parse_data(self, data):
        # Convert hex string to bytes
        bytes_data = bytes.fromhex(data)
        
        # Extract the first byte as Heart Rate
        heart_rate = int(bytes_data[0])
        
        # Extract the second byte as Force Detected
        force_detected = int(bytes_data[1])

        # Extract the third byte as '02 Sat'
        sat_value = int(bytes_data[2])

        return heart_rate, force_detected, sat_value

    def run(self):
        try:
            xbee = XBeeDevice(COM_PORT, baud_rate=BAUD_RATE)
            xbee.open()
            print("Waiting for data...")
            
            while not self.stop_event.is_set():
                message = xbee.read_data()
                if message:
                    router_id = str(message.remote_device.get_64bit_addr())
                    data = message.data.hex()  # Convert to hex string
                    heart_rate, force_detected, sat_value = self.parse_data(data)
                    self.data_received.emit(router_id, heart_rate, force_detected, sat_value)

        except Exception as e:
            print("Error:", e)

        finally:
            if xbee.is_open():
                xbee.close()

    def stop(self):
        self.stop_event.set()

class GUI(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("XBee Data Receiver")
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.layout = QVBoxLayout(self.central_widget)

        self.entries = {}  # Dictionary to store entries for each device

        self.create_labels()

        self.setGeometry(100, 100, 600, 400)  # Increased width for better visualization

    def create_labels(self):
        grid_layout = QGridLayout()
        self.layout.addLayout(grid_layout)

        for i in range(9):
            container = QWidget()
            container_layout = QVBoxLayout(container)
            labels = ["Router ID:", "Heart Rate:", "Force Detected:", "02 Sat:"]
            self.entries[str(i)] = {}

            for label in labels:
                widget_container = QWidget()
                widget_layout = QGridLayout(widget_container)
                widget_layout.addWidget(QLabel(label), 0, 0)
                self.entries[str(i)][label] = QLineEdit()
                self.entries[str(i)][label].setReadOnly(True)
                widget_layout.addWidget(self.entries[str(i)][label], 0, 1)
                container_layout.addWidget(widget_container)

            # Add border
            frame = QFrame()
            frame.setFrameShape(QFrame.HLine)
            frame.setFrameShadow(QFrame.Sunken)
            container_layout.addWidget(frame)

            grid_layout.addWidget(container, i // 3, i % 3)

    def update_text(self, router_id, heart_rate, force_detected, sat_value):
        # Find the next available entry or overwrite existing one if router_id already exists
        for i in range(9):
            key = str(i)
            if self.entries[key]["Router ID:"].text() == router_id:
                break
            if not self.entries[key]["Router ID:"].text():
                break
        else:
            return  # No available entry found

        self.entries[key]["Router ID:"].setText(router_id)
        self.entries[key]["Heart Rate:"].setText(str(heart_rate))
        self.entries[key]["Force Detected:"].setText(str(force_detected))
        self.entries[key]["02 Sat:"].setText(str(sat_value))
        
        # Change background color to red if stats is out of range
        if heart_rate < 50 or heart_rate > 120 or force_detected > 7 or sat_value < 85:
            for label in self.entries[key].values():
                label.setStyleSheet("background-color: red")
        # Change background color to yellow if stats is almost out of range
        elif heart_rate < 60 or heart_rate > 100 or force_detected > 4 or sat_value < 90:
            for label in self.entries[key].values():
                label.setStyleSheet("background-color: yellow")
        else:
            for label in self.entries[key].values():
                label.setStyleSheet("background-color: white")

def main():
    app = QApplication(sys.argv)
    gui = GUI()
    gui.show()
    serial_reader = SerialReader(gui)
    serial_reader.data_received.connect(gui.update_text)
    serial_reader.start()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
