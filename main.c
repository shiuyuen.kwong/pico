#include "FreeRTOS.h"
#include "task.h"

#include "pico/stdlib.h"

#include "i2c_wrapper.h"

#include "alarm_task.h"
#include "H3LIS200DL_task.h"
#include "spo2_task.h"
#include "xbee_sender_task.h"

int main(){
    stdio_init_all();
    i2c_wrapper__i2c0_init();

    h3lis200dl_task_startup();
    spo2_task__startup();
    xbee_sender__startup();
    alarm_task__startup();

    vTaskStartScheduler();
}

