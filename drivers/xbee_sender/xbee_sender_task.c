#include "xbee_sender_task.h"
#include "H3LIS200DL_task.h"
#include "spo2_task.h"

#include "FreeRTOS.h"
#include "task.h"

#include "hardware/uart.h"
#include "hardware/gpio.h"

#include <stdio.h>
#include <math.h>
#include <stddef.h>

typedef struct __attribute__((packed)) {
  int8_t avg_bpm;
  uint8_t total_gs;
  int8_t spo2;
} xbee_message_t;

////////////////////
/** DEBUG DEFINES */
////////////////////
extern int32_t heartRate; // heart rate value
extern int32_t spo2;      // SPO2 value

///////////////////
/** CONST VALUES */
///////////////////
const uart_inst_t* xbee_uart_port = uart1;
const uint16_t xbee_baud_rate = 9600;
const uint xbee_tx = 4;

///////////////////////
/** STATIC FUNCTIONS */
///////////////////////
static void grab_latest_data_to_be_sent_over_xbee(xbee_message_t *xbee_message_p) {
	xbee_message_p->avg_bpm = grab_avg_heart_rate();
	xbee_message_p->total_gs = h3lis200dl__get_latest_calculated_g();
	xbee_message_p->spo2 = grab_avg_spo2();
}

static uint64_t format_xbee_message(xbee_message_t *xbee_message_p) {
  uint64_t raw_message = 0;
  uint8_t raw_message_buffer[8]; // 64 bits

  raw_message |= xbee_message_p->avg_bpm; /** First 8 bits */
  raw_message |= ((uint64_t)xbee_message_p->total_gs << 8);
  raw_message |= ((uint64_t)xbee_message_p->spo2 << 16);
  return raw_message;
}

static void send_xbee_message(uint64_t raw_message) {
  size_t big_endian_iterator = sizeof(xbee_message_t); /** Do we have to align struct? idk */
  // printf("packet size: %d\n", big_endian_iterator);
  uint8_t *raw_message_u8_ptr = (uint8_t *)&raw_message;
  // printf("Raw: ");
  for (int i = 0; i < big_endian_iterator; i++) { /** Send message in big endian format */
    uart_putc_raw(xbee_uart_port, raw_message_u8_ptr[i]);
  }
}

static void xbee_sender__init(void) {
  uart_init(xbee_uart_port, xbee_baud_rate);
  gpio_set_function(xbee_tx, GPIO_FUNC_UART);
}

static void xbee_sender__task(void *param) {
  xbee_sender__init();
  while (1) {
    uint64_t raw_message = 0;
    xbee_message_t xbee_message;

    printf("HR: %d, SPO2: %d\n", heartRate, spo2);
    grab_latest_data_to_be_sent_over_xbee(&xbee_message);
    raw_message = format_xbee_message(&xbee_message);
    send_xbee_message(raw_message);
    vTaskDelay(1000);
  }
}

///////////////////////
/** PUBLIC FUNCTIONS */
///////////////////////


bool xbee_sender__startup(void) {
  bool ret = false;
  if (xTaskCreate(xbee_sender__task, "x_send", 2048 / sizeof(void *), NULL, 2, NULL)) {
    ret = true;
  }
  return ret;
}