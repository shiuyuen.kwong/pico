#pragma once

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

/************************* */
/** RANDOM ARDUINO DEFINES */
/************************* */
#define byte uint8_t
#define boolean bool

/******************** */
/** MAX3010X DEFINES  */
/******************** */
#define MAX30105_ADDRESS 0x57 // 7-bit I2C Address
// Note that MAX30102 has the same I2C address and Part ID

#define I2C_SPEED_STANDARD 100000
#define I2C_SPEED_FAST 400000

#define I2C_BUFFER_LENGTH 32
#define STORAGE_SIZE 4 // Each long is 4 bytes so limit this to fit on your micro

/******************** */
/** TYPESDEFS/STRUCTS */
/******************** */

typedef struct Record {
  uint32_t red[STORAGE_SIZE];
  uint32_t IR[STORAGE_SIZE];
  uint32_t green[STORAGE_SIZE];
  byte head;
  byte tail;
} sense_struct; // This is our circular buffer of readings from the sensor

typedef struct {
  uint8_t _i2caddr; // Should correspond to the MAX30105 address
  uint8_t revisionID;
  sense_struct sense;
  uint8_t activeLEDs;
} max30105_t;

void setup(max30105_t *max_instance, byte powerLevel, byte sampleAverage, byte ledMode, int sampleRate, int pulseWidth,
           int adcRange);

uint32_t getRed(max30105_t *max_instance);                        // Returns immediate red value
uint32_t getIR(max30105_t *max_instance);                         // Returns immediate IR value
uint32_t getGreen(max30105_t *max_instance);                      // Returns immediate green value
bool safeCheck(max30105_t *max_instance, uint8_t maxTimeToCheck); // Given a max amount of time, check for new data

bool read_register_mult(max30105_t *max_instance, uint8_t starting_slave_memory_address, uint8_t *buffer,
                        uint32_t number_of_bytes_to_transfer);
// Configuration
void softReset(max30105_t *max_instance);
void shutDown(max30105_t *max_instance);
void wakeUp(max30105_t *max_instance);
void readRevisionID(max30105_t *max_instance);
void bitMask(max30105_t *max_instance, uint8_t reg, uint8_t mask, uint8_t thing);

void setLEDMode(max30105_t *max_instance, uint8_t mode);

void setADCRange(max30105_t *max_instance, uint8_t adcRange);
void setSampleRate(max30105_t *max_instance, uint8_t sampleRate);
void setPulseWidth(max30105_t *max_instance, uint8_t pulseWidth);

void setPulseAmplitudeRed(max30105_t *max_instance, uint8_t amplitude);
void setPulseAmplitudeIR(max30105_t *max_instance, uint8_t amplitude);
void setPulseAmplitudeGreen(max30105_t *max_instance, uint8_t amplitude);
void setPulseAmplitudeProximity(max30105_t *max_instance, uint8_t amplitude);

void setProximityThreshold(max30105_t *max_instance, uint8_t threshMSB);

// Multi-led configuration mode (page 22)
void enableSlot(max30105_t *max_instance, uint8_t slotNumber,
                uint8_t device); // Given slot number, assign a device to slot
void disableSlots(max30105_t *max_instance);
// Data Collection

// Interrupts (page 13, 14)
uint8_t getINT1(max30105_t *max_instance);  // Returns the main interrupt group
uint8_t getINT2(max30105_t *max_instance);  // Returns the temp ready interrupt
void enableAFULL(max30105_t *max_instance); // Enable/disable individual interrupts
void disableAFULL(max30105_t *max_instance);
void enableDATARDY(max30105_t *max_instance);
void disableDATARDY(max30105_t *max_instance);
void enableALCOVF(max30105_t *max_instance);
void disableALCOVF(max30105_t *max_instance);
void enablePROXINT(max30105_t *max_instance);
void disablePROXINT(max30105_t *max_instance);
void enableDIETEMPRDY(max30105_t *max_instance);
void disableDIETEMPRDY(max30105_t *max_instance);

// FIFO Configuration (page 18)
void setFIFOAverage(max30105_t *max_instance, uint8_t samples);
void enableFIFORollover(max30105_t *max_instance);
void disableFIFORollover(max30105_t *max_instance);
void setFIFOAlmostFull(max30105_t *max_instance, uint8_t samples);

// FIFO Reading
uint16_t check(max30105_t *max_instance);        // Checks for new data and fills FIFO
uint8_t available(max30105_t *max_instance);     // Tells caller how many new samples are available (head - tail)
void nextSample(max30105_t *max_instance);       // Advances the tail of the sense array
uint32_t getFIFORed(max30105_t *max_instance);   // Returns the FIFO sample pointed to by tail
uint32_t getFIFOIR(max30105_t *max_instance);    // Returns the FIFO sample pointed to by tail
uint32_t getFIFOGreen(max30105_t *max_instance); // Returns the FIFO sample pointed to by tail

uint8_t getWritePointer(max30105_t *max_instance);
uint8_t getReadPointer(max30105_t *max_instance);
void clearFIFO(max30105_t *max_instance); // Sets the read/write pointers to zero

// Proximity Mode Interrupt Threshold
void setPROXINTTHRESH(max30105_t *max_instance, uint8_t val);

// Die Temperature
float readTemperature(max30105_t *max_instance);
float readTemperatureF(max30105_t *max_instance);

// Detecting ID/Revision
uint8_t getRevisionID(max30105_t *max_instance);
uint8_t readPartID(max30105_t *max_instance);

// Setup the IC with user selectable settings
// void setup(byte powerLevel = 0x1F, byte sampleAverage = 4, byte ledMode = 3, int sampleRate = 400, int pulseWidth =
// 411, int adcRange = 4096);

// Low-level I2C communication
uint8_t readRegister8(max30105_t *max_instance, uint8_t reg);
void writeRegister8(max30105_t *max_instance, uint8_t reg, uint8_t value);
