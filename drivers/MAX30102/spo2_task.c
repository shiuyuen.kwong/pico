#include "spo2_task.h"
#include "max3010x.h"
#include "heartrate.h"

#include "FreeRTOS.h"
#include "task.h"

#include <stdio.h>

#include "pico-sdk/src/common/pico_time/include/pico/time.h"

#define RATE_SIZE 4    // Increase this for more averaging. 4 is good.
byte rates[RATE_SIZE]; // Array of heart rates
byte rateSpot = 0;
int32_t lastBeat = 0; // Time at which the last beat occurred

static float beatsPerMinute;
static int32_t beatAvg;

static uint32_t irBuffer[100];  // infrared LED sensor data
static uint32_t redBuffer[100]; // red LED sensor data

int32_t bufferLength; // data length
int32_t spo2;         // SPO2 value
int32_t avg_spo2;
int8_t validSPO2;  // indicator to show if the SPO2 calculation is valid
int32_t heartRate; // heart rate value
int32_t avg_heart_rate;
int8_t validHeartRate; // indicator to show if the heart rate calculation is valid

int8_t grab_avg_spo2(void) { return avg_spo2; }

int8_t grab_avg_heart_rate(void) { return avg_heart_rate; }

void spo2_task(void *p) {
  max30105_t po_module_instance;
  max30105_t *po_mod_ptr = &po_module_instance;
  byte ledBrightness = 0xF; // Options: 0=Off to 255=50mA
  byte sampleAverage = 4;   // Options: 1, 2, 4, 8, 16, 32
  byte ledMode = 3;         // Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  int sampleRate = 200;     // Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  int pulseWidth = 411;     // Options: 69, 118, 215, 411
  int adcRange = 4096;      // Options: 2048, 4096, 8192, 16384

  setup(po_mod_ptr, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);
  uint64_t start_time = to_ms_since_boot(get_absolute_time());
  printf("start first 4 secsn");
  while (1) {
    bufferLength = 100; // buffer length of 100 stores 4 seconds of samples running at 25sps

    // read the first 100 samples, and determine the signal range
    for (byte i = 0; i < bufferLength; i++) {
      while (available(po_mod_ptr) == 0){ // do we have new data?
        check(po_mod_ptr);
        // printf("asdasdas\n");
      }    
      redBuffer[i] = getFIFORed(po_mod_ptr);
      irBuffer[i] = getFIFOIR(po_mod_ptr);
      nextSample(po_mod_ptr); // We're finished with this sample so move to next sample

      // printf(("red=%u\n"), redBuffer[i]);
      // printf(("ir=%u\n"), irBuffer[i]);
    }

    // calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
    maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate,
                                           &validHeartRate);
    // Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
    while (1) {
      // dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
      for (byte i = 25; i < 100; i++) {
        redBuffer[i - 25] = redBuffer[i];
        irBuffer[i - 25] = irBuffer[i];
      }

      // take 25 sets of samples before calculating the heart rate.
      for (byte i = 75; i < 100; i++) {
        // printf("H\n");

        while (available(po_mod_ptr) == 0){ // do we have new data?
          check(po_mod_ptr);
          // printf("dfgdfg\n");
        }               // Check the sensor for new data

        // printf("xcvcv\n");
        redBuffer[i] = getFIFORed(po_mod_ptr);
        irBuffer[i] = getFIFOIR(po_mod_ptr);
        if (checkForBeat(irBuffer[i]) == true) {
          printf("BEAT\n");
          // We sensed a beat!
          // printf("sensed a beat!\n");
          // turn_on_heartbeat_indicator_led();
          uint32_t current_time = to_ms_since_boot(get_absolute_time());
          uint32_t delta = current_time - lastBeat;
          lastBeat = current_time;

          beatsPerMinute = 60 / (delta / 1000.0);

          if (beatsPerMinute < 255 && beatsPerMinute > 20) {
            rates[rateSpot++] = (byte)beatsPerMinute; // Store this reading in the array
            rateSpot %= RATE_SIZE;                    // Wrap variable

            // Take average of readings
            beatAvg = 0;
            for (byte x = 0; x < RATE_SIZE; x++)
              beatAvg += rates[x];
            beatAvg /= RATE_SIZE;
          }
          printf("beatAvg: %d\n", beatAvg);
        }
        nextSample(po_mod_ptr); // We're finished with this sample so move to next sample

        // send samples and calculation result to terminal program through UART
        // printf(("red=%u\n"), redBuffer[0]);
        // printf("ir=%u\n", irBuffer[0]);

        // printf(", HR=%d\n", heartRate);

        // printf(", HRvalid=%d\n", validHeartRate);

        // printf(", SPO2=%u\n", spo2);

        // printf(", SPO2Valid=%d\n", validSPO2);
      }

      // After gathering 25 new samples recalculate HR and SP02
      maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate,
                                             &validHeartRate);

      if (validSPO2) {
        avg_spo2 = (avg_spo2 + spo2) / 2;
      }

      if (validHeartRate) {
        avg_heart_rate = (avg_heart_rate + heartRate) / 2;
      }
    }
  }
}

bool spo2_task__startup(void) { xTaskCreate(spo2_task, "spo2", 4096 / sizeof(void *), NULL, 1, NULL); }
