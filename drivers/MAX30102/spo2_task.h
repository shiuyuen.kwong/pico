#pragma once

#include "spo2.h"
#include <stdbool.h>
#include <stdio.h>

bool spo2_task__startup(void);
void spo2_task(void *p);
int8_t grab_avg_spo2(void);
int8_t grab_avg_heart_rate(void);