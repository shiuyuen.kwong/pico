#include "max3010x.h"
#include "pico-sdk/src/common/pico_time/include/pico/time.h"
#include "i2c_wrapper.h"


#include "FreeRTOS.h"
#include "task.h"

#define pri8x "0x%02hX\n"
#include <stdio.h>
#include <string.h>


// Status Registers
static const uint8_t MAX30105_INTSTAT1 = 0x00;
static const uint8_t MAX30105_INTSTAT2 = 0x01;
static const uint8_t MAX30105_INTENABLE1 = 0x02;
static const uint8_t MAX30105_INTENABLE2 = 0x03;

// FIFO Registers
static const uint8_t MAX30105_FIFOWRITEPTR = 0x04;
static const uint8_t MAX30105_FIFOOVERFLOW = 0x05;
static const uint8_t MAX30105_FIFOREADPTR = 0x06;
static const uint8_t MAX30105_FIFODATA = 0x07;

// Configuration Registers
static const uint8_t MAX30105_FIFOCONFIG = 0x08;
static const uint8_t MAX30105_MODECONFIG = 0x09;
static const uint8_t MAX30105_PARTICLECONFIG = 0x0A; // Note, sometimes listed as "SPO2" config in datasheet (pg. 11)
static const uint8_t MAX30105_LED1_PULSEAMP = 0x0C;
static const uint8_t MAX30105_LED2_PULSEAMP = 0x0D;
static const uint8_t MAX30105_LED3_PULSEAMP = 0x0E;
static const uint8_t MAX30105_LED_PROX_AMP = 0x10;
static const uint8_t MAX30105_MULTILEDCONFIG1 = 0x11;
static const uint8_t MAX30105_MULTILEDCONFIG2 = 0x12;

// Die Temperature Registers
static const uint8_t MAX30105_DIETEMPINT = 0x1F;
static const uint8_t MAX30105_DIETEMPFRAC = 0x20;
static const uint8_t MAX30105_DIETEMPCONFIG = 0x21;

// Proximity Function Registers
static const uint8_t MAX30105_PROXINTTHRESH = 0x30;

// Part ID Registers
static const uint8_t MAX30105_REVISIONID = 0xFE;
static const uint8_t MAX30105_PARTID = 0xFF; // Should always be 0x15. Identical to MAX30102.

// MAX30105 Commands
// Interrupt configuration (pg 13, 14)
static const uint8_t MAX30105_INT_A_FULL_MASK = (byte)~0b10000000;
static const uint8_t MAX30105_INT_A_FULL_ENABLE = 0x80;
static const uint8_t MAX30105_INT_A_FULL_DISABLE = 0x00;

static const uint8_t MAX30105_INT_DATA_RDY_MASK = (byte)~0b01000000;
static const uint8_t MAX30105_INT_DATA_RDY_ENABLE = 0x40;
static const uint8_t MAX30105_INT_DATA_RDY_DISABLE = 0x00;

static const uint8_t MAX30105_INT_ALC_OVF_MASK = (byte)~0b00100000;
static const uint8_t MAX30105_INT_ALC_OVF_ENABLE = 0x20;
static const uint8_t MAX30105_INT_ALC_OVF_DISABLE = 0x00;

static const uint8_t MAX30105_INT_PROX_INT_MASK = (byte)~0b00010000;
static const uint8_t MAX30105_INT_PROX_INT_ENABLE = 0x10;
static const uint8_t MAX30105_INT_PROX_INT_DISABLE = 0x00;

static const uint8_t MAX30105_INT_DIE_TEMP_RDY_MASK = (byte)~0b00000010;
static const uint8_t MAX30105_INT_DIE_TEMP_RDY_ENABLE = 0x02;
static const uint8_t MAX30105_INT_DIE_TEMP_RDY_DISABLE = 0x00;

static const uint8_t MAX30105_SAMPLEAVG_MASK = (byte)~0b11100000;
static const uint8_t MAX30105_SAMPLEAVG_1 = 0x00;
static const uint8_t MAX30105_SAMPLEAVG_2 = 0x20;
static const uint8_t MAX30105_SAMPLEAVG_4 = 0x40;
static const uint8_t MAX30105_SAMPLEAVG_8 = 0x60;
static const uint8_t MAX30105_SAMPLEAVG_16 = 0x80;
static const uint8_t MAX30105_SAMPLEAVG_32 = 0xA0;

static const uint8_t MAX30105_ROLLOVER_MASK = 0xEF;
static const uint8_t MAX30105_ROLLOVER_ENABLE = 0x10;
static const uint8_t MAX30105_ROLLOVER_DISABLE = 0x00;

static const uint8_t MAX30105_A_FULL_MASK = 0xF0;

// Mode configuration commands (page 19)
static const uint8_t MAX30105_SHUTDOWN_MASK = 0x7F;
static const uint8_t MAX30105_SHUTDOWN = 0x80;
static const uint8_t MAX30105_WAKEUP = 0x00;

static const uint8_t MAX30105_RESET_MASK = 0xBF;
static const uint8_t MAX30105_RESET = 0x40;

static const uint8_t MAX30105_MODE_MASK = 0xF8;
static const uint8_t MAX30105_MODE_REDONLY = 0x02;
static const uint8_t MAX30105_MODE_REDIRONLY = 0x03;
static const uint8_t MAX30105_MODE_MULTILED = 0x07;

// Particle sensing configuration commands (pgs 19-20)
static const uint8_t MAX30105_ADCRANGE_MASK = 0x9F;
static const uint8_t MAX30105_ADCRANGE_2048 = 0x00;
static const uint8_t MAX30105_ADCRANGE_4096 = 0x20;
static const uint8_t MAX30105_ADCRANGE_8192 = 0x40;
static const uint8_t MAX30105_ADCRANGE_16384 = 0x60;

static const uint8_t MAX30105_SAMPLERATE_MASK = 0xE3;
static const uint8_t MAX30105_SAMPLERATE_50 = 0x00;
static const uint8_t MAX30105_SAMPLERATE_100 = 0x04;
static const uint8_t MAX30105_SAMPLERATE_200 = 0x08;
static const uint8_t MAX30105_SAMPLERATE_400 = 0x0C;
static const uint8_t MAX30105_SAMPLERATE_800 = 0x10;
static const uint8_t MAX30105_SAMPLERATE_1000 = 0x14;
static const uint8_t MAX30105_SAMPLERATE_1600 = 0x18;
static const uint8_t MAX30105_SAMPLERATE_3200 = 0x1C;

static const uint8_t MAX30105_PULSEWIDTH_MASK = 0xFC;
static const uint8_t MAX30105_PULSEWIDTH_69 = 0x00;
static const uint8_t MAX30105_PULSEWIDTH_118 = 0x01;
static const uint8_t MAX30105_PULSEWIDTH_215 = 0x02;
static const uint8_t MAX30105_PULSEWIDTH_411 = 0x03;

// Multi-LED Mode configuration (pg 22)
static const uint8_t MAX30105_SLOT1_MASK = 0xF8;
static const uint8_t MAX30105_SLOT2_MASK = 0x8F;
static const uint8_t MAX30105_SLOT3_MASK = 0xF8;
static const uint8_t MAX30105_SLOT4_MASK = 0x8F;

static const uint8_t SLOT_NONE = 0x00;
static const uint8_t SLOT_RED_LED = 0x01;
static const uint8_t SLOT_IR_LED = 0x02;
static const uint8_t SLOT_GREEN_LED = 0x03;
static const uint8_t SLOT_NONE_PILOT = 0x04;
static const uint8_t SLOT_RED_PILOT = 0x05;
static const uint8_t SLOT_IR_PILOT = 0x06;
static const uint8_t SLOT_GREEN_PILOT = 0x07;

static const uint8_t MAX_30105_EXPECTEDPARTID = 0x15;

void setup(max30105_t *max_instance, byte powerLevel, byte sampleAverage, byte ledMode, int sampleRate, int pulseWidth,
           int adcRange) {
  softReset(max_instance); // Reset all configuration, threshold, and data registers to POR values
  printf("softreset DONE\n");

  // FIFO Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // The chip will average multiple samples of same type together if you wish
  if (sampleAverage == 1)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_1); // No averaging per FIFO record
  else if (sampleAverage == 2)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_2);
  else if (sampleAverage == 4)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_4);
  else if (sampleAverage == 8)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_8);
  else if (sampleAverage == 16)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_16);
  else if (sampleAverage == 32)
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_32);
  else
    setFIFOAverage(max_instance, MAX30105_SAMPLEAVG_4);

  printf("setFIFOAvg DONE\n");

  // setFIFOAlmostFull(2); //Set to 30 samples to trigger an 'Almost Full' interrupt
  enableFIFORollover(max_instance); // Allow FIFO to wrap/roll over
  printf("enableFIFORollover DONE\n");

  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  // Mode Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if (ledMode == 3)
    setLEDMode(max_instance, MAX30105_MODE_MULTILED); // Watch all three LED channels
  else if (ledMode == 2)
    setLEDMode(max_instance, MAX30105_MODE_REDIRONLY); // Red and IR
  else
    setLEDMode(max_instance, MAX30105_MODE_REDONLY); // Red only
  max_instance->activeLEDs = ledMode;                // Used to control how many bytes to read from FIFO buffer
  uint8_t config_reg_readback = readRegister8(max_instance, MAX30105_MODECONFIG);
  printf("setLEDMode DONE config: 0x%02x\n", config_reg_readback);

  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  // Particle Sensing Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if (adcRange < 4096)
    setADCRange(max_instance, MAX30105_ADCRANGE_2048); // 7.81pA per LSB
  else if (adcRange < 8192)
    setADCRange(max_instance, MAX30105_ADCRANGE_4096); // 15.63pA per LSB
  else if (adcRange < 16384)
    setADCRange(max_instance, MAX30105_ADCRANGE_8192); // 31.25pA per LSB
  else if (adcRange == 16384)
    setADCRange(max_instance, MAX30105_ADCRANGE_16384); // 62.5pA per LSB
  else
    setADCRange(max_instance, MAX30105_ADCRANGE_2048);
  printf("setADCRange DONE\n");

  if (sampleRate < 100)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_50); // Take 50 samples per second
  else if (sampleRate < 200)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_100);
  else if (sampleRate < 400)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_200);
  else if (sampleRate < 800)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_400);
  else if (sampleRate < 1000)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_800);
  else if (sampleRate < 1600)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_1000);
  else if (sampleRate < 3200)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_1600);
  else if (sampleRate == 3200)
    setSampleRate(max_instance, MAX30105_SAMPLERATE_3200);
  else
    setSampleRate(max_instance, MAX30105_SAMPLERATE_50);
  printf("setSampleRate %d DONE\n", sampleRate);

  // The longer the pulse width the longer range of detection you'll have
  // At 69us and 0.4mA it's about 2 inches
  // At 411us and 0.4mA it's about 6 inches
  if (pulseWidth < 118)
    setPulseWidth(max_instance, MAX30105_PULSEWIDTH_69); // Page 26, Gets us 15 bit resolution
  else if (pulseWidth < 215)
    setPulseWidth(max_instance, MAX30105_PULSEWIDTH_118); // 16 bit resolution
  else if (pulseWidth < 411)
    setPulseWidth(max_instance, MAX30105_PULSEWIDTH_215); // 17 bit resolution
  else if (pulseWidth == 411)
    setPulseWidth(max_instance, MAX30105_PULSEWIDTH_411); // 18 bit resolution
  else
    setPulseWidth(max_instance, MAX30105_PULSEWIDTH_69);
  printf("setPulseWidth DONE\n");

  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  // LED Pulse Amplitude Configuration
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // Default is 0x1F which gets us 6.4mA
  // powerLevel = 0x02, 0.4mA - Presence detection of ~4 inch
  // powerLevel = 0x1F, 6.4mA - Presence detection of ~8 inch
  // powerLevel = 0x7F, 25.4mA - Presence detection of ~8 inch
  // powerLevel = 0xFF, 50.0mA - Presence detection of ~12 inch

  setPulseAmplitudeRed(max_instance, powerLevel);
  setPulseAmplitudeIR(max_instance, powerLevel);
  setPulseAmplitudeGreen(max_instance, powerLevel);
  // setPulseAmplitudeProximity(max_instance, powerLevel);
  printf("setPulseAmplitude DONE\n");

  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  // Multi-LED Mode Configuration, Enable the reading of the three LEDs
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  enableSlot(max_instance, 1, SLOT_RED_LED);
  enableSlot(max_instance, 2, SLOT_IR_LED);
  // if (ledMode > 2)
  //   enableSlot(max_instance, 3, SLOT_GREEN_LED);
  printf("enableSlot DONE\n");

  printf("reg dump:\n");
  for (int i = 0x08; i <= 0x12; i++) {
    if (i != 0x0B && i != 0x0E && i != 0x0F) {
      printf("reg 0x%02x: 0b", i);
      uint8_t reg_val = readRegister8(max_instance, i);
      for (int i = 0; i < 8; i++) {
        printf("%d", (reg_val >> ((7 - i))) & 0x1);
      }
      printf("\n");
    }
  }

  // enableSlot(1, SLOT_RED_PILOT);
  // enableSlot(2, SLOT_IR_PILOT);
  // enableSlot(3, SLOT_GREEN_PILOT);
  //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  clearFIFO(max_instance); // Reset the FIFO before we begin checking the sensor
}

uint32_t getRed(max30105_t *max_instance) {
  if (safeCheck(max_instance, 250)) {
    return (max_instance->sense.red[max_instance->sense.head]);
  } else {
    return (0); // Sensor failed to find new data
  }
}

uint32_t getIR(max30105_t *max_instance) {
  // Check the sensor for new data for 250ms
  if (safeCheck(max_instance, 250))
    return (max_instance->sense.IR[max_instance->sense.head]);
  else
    return (0); // Sensor failed to find new data
}

uint32_t getGreen(max30105_t *max_instance) {
  // Check the sensor for new data for 250ms
  if (safeCheck(max_instance, 250))
    return (max_instance->sense.green[max_instance->sense.head]);
  else
    return (0); // Sensor failed to find new data
}

bool safeCheck(max30105_t *max_instance, uint8_t maxTimeToCheck) {
  uint64_t markTime = to_ms_since_boot(get_absolute_time());

  while (1) {
    if (to_ms_since_boot(get_absolute_time()) - markTime > maxTimeToCheck) {
      return (false);
    }

    if (check(max_instance) == true) { // We found new data!
      return (true);
    }

    vTaskDelay(1);
  }
}

// Configuration
void softReset(max30105_t *max_instance) {
  printf("softReset START\n");
  bitMask(max_instance, MAX30105_MODECONFIG, MAX30105_RESET_MASK, MAX30105_RESET);

  // Poll for bit to clear, reset is then complete
  // Timeout after 100ms
  unsigned long startTime = to_ms_since_boot(get_absolute_time());
  printf("attempting to reset/config module\n");
  while (to_ms_since_boot(get_absolute_time()) - startTime < 100) {
    uint8_t response = readRegister8(max_instance, MAX30105_MODECONFIG);
    if ((response & MAX30105_RESET) == 0) {
      printf("config DONE\n");
      break; // We're done!
    }
    vTaskDelay(1); // Let's not over burden the I2C bus
  }
}
void shutDown(max30105_t *max_instance) {
  // Put IC into low power mode (datasheet pg. 19)
  // During shutdown the IC will continue to respond to I2C commands but will
  // not update with or take new readings (such as temperature)
  bitMask(max_instance, MAX30105_MODECONFIG, MAX30105_SHUTDOWN_MASK, MAX30105_SHUTDOWN);
}
void wakeUp(max30105_t *max_instance) {
  // Pull IC out of low power mode (datasheet pg. 19)
  bitMask(max_instance, MAX30105_MODECONFIG, MAX30105_SHUTDOWN_MASK, MAX30105_WAKEUP);
}

void readRevisionID(max30105_t *max_instance) {
  max_instance->revisionID = readRegister8(max_instance, MAX30105_REVISIONID);
  printf("revision ID: " pri8x, max_instance->revisionID);
}

void bitMask(max30105_t *max_instance, uint8_t reg, uint8_t mask, uint8_t thing) {
  // Grab current register context
  uint8_t originalContents = readRegister8(max_instance, reg);

  // Zero-out the portions of the register we're interested in
  originalContents = originalContents & mask;

  // Change contents
  writeRegister8(max_instance, reg, originalContents | thing);
  if (mask == MAX30105_SAMPLERATE_MASK) {
    printf("wrote 0x%02x to reg\n", originalContents | thing);
  }
}

void setLEDMode(max30105_t *max_instance, uint8_t mode) {
  // Set which LEDs are used for sampling -- Red only, RED+IR only, or custom.
  // See datasheet, page 19
  bitMask(max_instance, MAX30105_MODECONFIG, MAX30105_MODE_MASK, mode);
}

void setADCRange(max30105_t *max_instance, uint8_t adcRange) {
  // adcRange: one of MAX30105_ADCRANGE_2048, _4096, _8192, _16384
  bitMask(max_instance, MAX30105_PARTICLECONFIG, MAX30105_ADCRANGE_MASK, adcRange);
}

void setSampleRate(max30105_t *max_instance, uint8_t sampleRate) {
  // sampleRate: one of MAX30105_SAMPLERATE_50, _100, _200, _400, _800, _1000, _1600, _3200
  printf("sampleRate config: 0x%02x\n", sampleRate);
  bitMask(max_instance, MAX30105_PARTICLECONFIG, MAX30105_SAMPLERATE_MASK, sampleRate);
}
void setPulseWidth(max30105_t *max_instance, uint8_t pulseWidth) {
  // pulseWidth: one of MAX30105_PULSEWIDTH_69, _188, _215, _411
  bitMask(max_instance, MAX30105_PARTICLECONFIG, MAX30105_PULSEWIDTH_MASK, pulseWidth);
}

void setPulseAmplitudeRed(max30105_t *max_instance, uint8_t amplitude) {
    writeRegister8(max_instance, MAX30105_LED1_PULSEAMP, amplitude);
}
void setPulseAmplitudeIR(max30105_t *max_instance, uint8_t amplitude) {
    writeRegister8(max_instance, MAX30105_LED2_PULSEAMP, amplitude);
}
void setPulseAmplitudeGreen(max30105_t *max_instance, uint8_t amplitude) {
  //   writeRegister8(max_instance, MAX30105_LED3_PULSEAMP, amplitude);
}
void setPulseAmplitudeProximity(max30105_t *max_instance, uint8_t amplitude) {
    writeRegister8(max_instance, MAX30105_LED_PROX_AMP, amplitude);
}

void setProximityThreshold(max30105_t *max_instance, uint8_t threshMSB) {
  // Set the IR ADC count that will trigger the beginning of particle-sensing mode.
  // The threshMSB signifies only the 8 most significant-bits of the ADC count.
  // See datasheet, page 24.
    writeRegister8(max_instance, MAX30105_PROXINTTHRESH, threshMSB);
}

// Multi-led configuration mode (page 22)
void enableSlot(max30105_t *max_instance, uint8_t slotNumber, uint8_t device) {
  uint8_t originalContents;

  switch (slotNumber) {
  case (1):
    bitMask(max_instance, MAX30105_MULTILEDCONFIG1, MAX30105_SLOT1_MASK, device);
    break;
  case (2):
    bitMask(max_instance, MAX30105_MULTILEDCONFIG1, MAX30105_SLOT2_MASK, device << 4);
    break;
  case (3):
    bitMask(max_instance, MAX30105_MULTILEDCONFIG2, MAX30105_SLOT3_MASK, device);
    break;
  case (4):
    bitMask(max_instance, MAX30105_MULTILEDCONFIG2, MAX30105_SLOT4_MASK, device << 4);
    break;
  default:
    // Shouldn't be here!
    break;
  }
}

void disableSlots(max30105_t *max_instance) {
    writeRegister8(max_instance, MAX30105_MULTILEDCONFIG1, 0);
    writeRegister8(max_instance, MAX30105_MULTILEDCONFIG2, 0);
}

// Data Collection

// Interrupts (page 13, 14)
// Returns the main interrupt group
uint8_t getINT1(max30105_t *max_instance) { return (readRegister8(max_instance, MAX30105_INTSTAT1)); }

// Returns the temp ready interrupt
uint8_t getINT2(max30105_t *max_instance) { return (readRegister8(max_instance, MAX30105_INTSTAT2)); }

// Enable/disable individual interrupts
void enableAFULL(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_A_FULL_MASK, MAX30105_INT_A_FULL_ENABLE);
}

void disableAFULL(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_A_FULL_MASK, MAX30105_INT_A_FULL_DISABLE);
}
void enableDATARDY(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_DATA_RDY_MASK, MAX30105_INT_DATA_RDY_ENABLE);
}
void disableDATARDY(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_DATA_RDY_MASK, MAX30105_INT_DATA_RDY_DISABLE);
}
void enableALCOVF(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_ALC_OVF_MASK, MAX30105_INT_ALC_OVF_ENABLE);
}
void disableALCOVF(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_ALC_OVF_MASK, MAX30105_INT_ALC_OVF_DISABLE);
}
void enablePROXINT(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_PROX_INT_MASK, MAX30105_INT_PROX_INT_ENABLE);
}
void disablePROXINT(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE1, MAX30105_INT_PROX_INT_MASK, MAX30105_INT_PROX_INT_DISABLE);
}
void enableDIETEMPRDY(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE2, MAX30105_INT_DIE_TEMP_RDY_MASK, MAX30105_INT_DIE_TEMP_RDY_ENABLE);
}
void disableDIETEMPRDY(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_INTENABLE2, MAX30105_INT_DIE_TEMP_RDY_MASK, MAX30105_INT_DIE_TEMP_RDY_DISABLE);
}

// FIFO Configuration (page 18)
void setFIFOAverage(max30105_t *max_instance, uint8_t numberOfSamples) {
  bitMask(max_instance, MAX30105_FIFOCONFIG, MAX30105_SAMPLEAVG_MASK, numberOfSamples);
}
void enableFIFORollover(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_FIFOCONFIG, MAX30105_ROLLOVER_MASK, MAX30105_ROLLOVER_ENABLE);
}
void disableFIFORollover(max30105_t *max_instance) {
  bitMask(max_instance, MAX30105_FIFOCONFIG, MAX30105_ROLLOVER_MASK, MAX30105_ROLLOVER_DISABLE);
}
void setFIFOAlmostFull(max30105_t *max_instance, uint8_t samples) {
  bitMask(max_instance, MAX30105_FIFOCONFIG, MAX30105_A_FULL_MASK, samples);
}

// FIFO Reading
uint16_t check(max30105_t *max_instance) {
  // Read register FIDO_DATA in (3-byte * number of active LED) chunks
  // Until FIFO_RD_PTR = FIFO_WR_PTR

  uint8_t readPointer = getReadPointer(max_instance);
  uint8_t writePointer = getWritePointer(max_instance);

  int numberOfSamples = 0;

  // Do we have new data?
  if (readPointer != writePointer) {
    // Calculate the number of readings we need to get from sensor
    numberOfSamples = writePointer - readPointer;
    if (numberOfSamples < 0)
      numberOfSamples += 32; // Wrap condition

    // We now have the number of readings, now calc bytes to read
    // For this example we are just doing Red and IR (3 bytes each)
    int bytesLeftToRead = numberOfSamples * max_instance->activeLEDs * 3;

    // We may need to read as many as 288 bytes so we read in blocks no larger than I2C_BUFFER_LENGTH
    // I2C_BUFFER_LENGTH changes based on the platform. 64 bytes for SAMD21, 32 bytes for Uno.
    // Wire.requestFrom() is limited to BUFFER_LENGTH which is 32 on the Uno
    while (bytesLeftToRead > 0) {
      int toGet = bytesLeftToRead;
      uint8_t tmp_buf[I2C_BUFFER_LENGTH];
      int tmp_index = 0;

      if (toGet > I2C_BUFFER_LENGTH) {
        // If toGet is 32 this is bad because we read 6 bytes (Red+IR * 3 = 6) at a time
        // 32 % 6 = 2 left over. We don't want to request 32 bytes, we want to request 30.
        // 32 % 9 (Red+IR+GREEN) = 5 left over. We want to request 27.

        toGet = I2C_BUFFER_LENGTH -
                (I2C_BUFFER_LENGTH %
                 (max_instance->activeLEDs * 3)); // Trim toGet to be a multiple of the samples we need to read
      }

      bytesLeftToRead -= toGet;

      // Request toGet number of bytes from sensor
      read_register_mult(max_instance, MAX30105_FIFODATA, tmp_buf, toGet);

      while (toGet > 0) {
        max_instance->sense.head++;               // Advance the head of the storage struct
        max_instance->sense.head %= STORAGE_SIZE; // Wrap condition

        byte temp[sizeof(uint32_t)]; // Array of 4 bytes that we will convert into long
        uint32_t tempLong;

        // Burst read three bytes - RED
        temp[3] = 0;
        {
          temp[2] = tmp_buf[tmp_index++];
          temp[1] = tmp_buf[tmp_index++];
          temp[0] = tmp_buf[tmp_index++];
        }

        // Convert array to long
        memcpy(&tempLong, temp, sizeof(tempLong));

        tempLong &= 0x3FFFF; // Zero out all but 18 bits

        max_instance->sense.red[max_instance->sense.head] = tempLong; // Store this reading into the sense array

        if (max_instance->activeLEDs > 1) {
          // Burst read three more bytes - IR
          temp[3] = 0;
          {
            temp[2] = tmp_buf[tmp_index++];
            temp[1] = tmp_buf[tmp_index++];
            temp[0] = tmp_buf[tmp_index++];
          }

          // Convert array to long
          memcpy(&tempLong, temp, sizeof(tempLong));

          tempLong &= 0x3FFFF; // Zero out all but 18 bits

          max_instance->sense.IR[max_instance->sense.head] = tempLong;
        }

        if (max_instance->activeLEDs > 2) {
          // Burst read three more bytes - Green
          temp[3] = 0;
          {
            temp[2] = tmp_buf[tmp_index++];
            temp[1] = tmp_buf[tmp_index++];
            temp[0] = tmp_buf[tmp_index++];
          }

          // Convert array to long
          memcpy(&tempLong, temp, sizeof(tempLong));

          tempLong &= 0x3FFFF; // Zero out all but 18 bits

          max_instance->sense.green[max_instance->sense.head] = tempLong;
        }

        toGet -= max_instance->activeLEDs * 3;
      }

    } // End while (bytesLeftToRead > 0)

  } // End readPtr != writePtr

  // printf("num of samples: %d\n", numberOfSamples);
  return (numberOfSamples); // Let the world know how much new data we found
}

uint8_t available(max30105_t *max_instance) {
  int8_t numberOfSamples = max_instance->sense.head - max_instance->sense.tail;
  if (numberOfSamples < 0)
    numberOfSamples += STORAGE_SIZE;

  return (numberOfSamples);
}

void nextSample(max30105_t *max_instance) {
  if (available(max_instance)) // Only advance the tail if new data is available
  {
    max_instance->sense.tail++;
    max_instance->sense.tail %= STORAGE_SIZE; // Wrap condition
  }
}
uint32_t getFIFORed(max30105_t *max_instance) { return (max_instance->sense.red[max_instance->sense.tail]); }
uint32_t getFIFOIR(max30105_t *max_instance) { return (max_instance->sense.IR[max_instance->sense.tail]); }
uint32_t getFIFOGreen(max30105_t *max_instance) { return (max_instance->sense.green[max_instance->sense.tail]); }

uint8_t getWritePointer(max30105_t *max_instance) { return (readRegister8(max_instance, MAX30105_FIFOWRITEPTR)); }

uint8_t getReadPointer(max30105_t *max_instance) { return (readRegister8(max_instance, MAX30105_FIFOREADPTR)); }

void clearFIFO(max30105_t *max_instance) {
    writeRegister8(max_instance, MAX30105_FIFOWRITEPTR, 0);
    writeRegister8(max_instance, MAX30105_FIFOOVERFLOW, 0);
    writeRegister8(max_instance, MAX30105_FIFOREADPTR, 0);
}

// Proximity Mode Interrupt Threshold
void setPROXINTTHRESH(max30105_t *max_instance, uint8_t val) {
    writeRegister8(max_instance, MAX30105_PROXINTTHRESH, val);
}

// Die Temperature
float readTemperature(max30105_t *max_instance) {
  // DIE_TEMP_RDY interrupt must be enabled
  // See issue 19: https://github.com/sparkfun/SparkFun_MAX3010x_Sensor_Library/issues/19

  // Step 1: Config die temperature register to take 1 temperature sample
    writeRegister8(max_instance, MAX30105_DIETEMPCONFIG, 0x01);

  // Poll for bit to clear, reading is then complete
  // Timeout after 100ms
  unsigned long startTime = to_ms_since_boot(get_absolute_time());
  while (to_ms_since_boot(get_absolute_time()) - startTime < 100) {
    // uint8_t response = readRegister8(_i2caddr, MAX30105_DIETEMPCONFIG); //Original way
    // if ((response & 0x01) == 0) break; //We're done!

    // Check to see if DIE_TEMP_RDY interrupt is set
    uint8_t response = readRegister8(max_instance, MAX30105_INTSTAT2);
    if ((response & MAX30105_INT_DIE_TEMP_RDY_ENABLE) > 0)
      break;  // We're done!
    vTaskDelay(1); // Let's not over burden the I2C bus
  }
  // TODO How do we want to fail? With what type of error?
  //? if(millis() - startTime >= 100) return(-999.0);

  // Step 2: Read die temperature register (integer)
  int8_t tempInt = readRegister8(max_instance, MAX30105_DIETEMPINT);
  uint8_t tempFrac =
      readRegister8(max_instance, MAX30105_DIETEMPFRAC); // Causes the clearing of the DIE_TEMP_RDY interrupt

  // Step 3: Calculate temperature (datasheet pg. 23)
  return (float)tempInt + ((float)tempFrac * 0.0625);
}
float readTemperatureF(max30105_t *max_instance) {
  float temp = readTemperature(max_instance);

  if (temp != -999.0)
    temp = temp * 1.8 + 32.0;

  return (temp);
}

// Detecting ID/Revision
uint8_t getRevisionID(max30105_t *max_instance) { return max_instance->revisionID; }
uint8_t readPartID(max30105_t *max_instance) { return readRegister8(max_instance, MAX30105_PARTID); }

// Setup the IC with user selectable settings
// void setup(byte powerLevel = 0x1F, byte sampleAverage = 4, byte ledMode = 3, int sampleRate = 400, int pulseWidth =
// 411, int adcRange = 4096);

// Low-level I2C communication
uint8_t readRegister8(max30105_t *max_instance, uint8_t reg) {
  uint8_t data = 0;
  uint8_t slave_address = MAX30105_ADDRESS;
  i2c_wrapper__i2c0_read(slave_address, reg, &data, 1);
  return data;
}

void writeRegister8(max30105_t *max_instance, uint8_t reg, uint8_t value) {
  i2c_wrapper__i2c0_write_single(MAX30105_ADDRESS, reg, value);
}

bool read_register_mult(max30105_t *max_instance, uint8_t starting_slave_memory_address, uint8_t *buffer,
                        uint32_t number_of_bytes_to_transfer) {
  i2c_wrapper__i2c0_read(MAX30105_ADDRESS, starting_slave_memory_address, buffer, number_of_bytes_to_transfer);
}
