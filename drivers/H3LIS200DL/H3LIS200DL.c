#include "H3LIS200DL.h"

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "i2c_wrapper.h"

static const uint8_t h3lis200dl_slave_addr = 0x19;
static const uint8_t h3lis200dl_who_am_i_reg_addr = 0x0F;
static const uint8_t h3lis200dl_who_am_i_reg_val = 0x32;

static const uint8_t h3lis200dl_ctrl_reg1_addr = 0x20;
static const uint8_t h3lis200dl_ctrl_reg2_addr = 0x21;
static const uint8_t h3lis200dl_ctrl_reg3_addr = 0x22;
static const uint8_t h3lis200dl_ctrl_reg4_addr = 0x23;
static const uint8_t h3lis200dl_ctrl_reg5_addr = 0x24;

static const uint8_t h3lis200dl_status_reg_addr = 0x27;
static const uint8_t h3lis200dl_out_x_addr = 0x29;
static const uint8_t h3lis200dl_out_y_addr = 0x2B;
static const uint8_t h3lis200dl_out_z_addr = 0x2D;

static const uint8_t h3lis200dl_full_scale = 100;

static uint8_t calculated_g;
static h3lis200dl_s* h3lis200dl_data;

static void h3lis200dl_calculate_total_g(void);
static bool h3lis200dl__config(void);
static bool h3lis200dl__who_am_i_is_correct(void);
static bool h3lis200dl__read_raw(uint8_t *raw_data);

bool h3lis200dl__init(void){
    bool config_status = false;
    if (h3lis200dl__who_am_i_is_correct()){
        config_status = h3lis200dl__config();
        memset(h3lis200dl_data, 0, sizeof(h3lis200dl_s));
    }

    return config_status;
}

bool h3lis200dl__read(void){
    uint8_t raw_data[3] = {0};
    const bool status = h3lis200dl__read_raw(raw_data);

    if (status) {
        const float x = (float)(int8_t)raw_data[0] * h3lis200dl_full_scale / 128;
        const float y = (float)(int8_t)raw_data[1] * h3lis200dl_full_scale / 128;
        const float z = (float)(int8_t)raw_data[2] * h3lis200dl_full_scale / 128;

        const float temp_g = sqrtf(x * x + y * y + z * z);
        if(temp_g != 0){
            calculated_g = temp_g;
        }
    }
    
    return status;
}

uint8_t h3lis200dl__get_data(void){
    return calculated_g;
}

static bool h3lis200dl__who_am_i_is_correct(void){
    const uint8_t one_byte = 1;

    uint8_t who_am_i_value;
    i2c_wrapper__i2c0_read(h3lis200dl_slave_addr, h3lis200dl_who_am_i_reg_addr, &who_am_i_value, one_byte);
    return who_am_i_value == h3lis200dl_who_am_i_reg_val;
}

static bool h3lis200dl__config(void){
    const uint8_t power_mode = 0b001 << 5;
    const uint8_t data_rate = 0b11 << 3;
    const uint8_t xyz = 0b111;
    const uint8_t write_buffer = power_mode | data_rate | xyz;

    const bool config_status = i2c_wrapper__i2c0_write_single(h3lis200dl_slave_addr, h3lis200dl_ctrl_reg1_addr, write_buffer);

    return config_status;
}

static bool h3lis200dl__read_raw(uint8_t *raw_data) {
    uint8_t raw_data_buffer[6] = {};
    const bool status =
        i2c_wrapper__i2c0_read(h3lis200dl_slave_addr, h3lis200dl_out_x_addr, raw_data_buffer, 6);

    raw_data[0] = raw_data_buffer[0];
    raw_data[1] = raw_data_buffer[2];
    raw_data[2] = raw_data_buffer[4];

    return status;
}

uint8_t h3lis200dl__who_am_i(void){
    uint8_t who_am_i_value;
    i2c_wrapper__i2c0_read(h3lis200dl_slave_addr, h3lis200dl_who_am_i_reg_addr, &who_am_i_value, 1);
    return who_am_i_value;
}

void h3lis200dl__dump(void){
    uint8_t ctrl_reg1;
    i2c_wrapper__i2c0_read(h3lis200dl_slave_addr, h3lis200dl_ctrl_reg1_addr, &ctrl_reg1, 1);
    printf("ctrl_reg1: %x\n", ctrl_reg1);
}