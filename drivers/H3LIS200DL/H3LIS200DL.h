#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	float x;
	float y;
	float z;
} h3lis200dl_s;

bool h3lis200dl__init(void);

bool h3lis200dl__read(void);

uint8_t h3lis200dl__get_data(void);

uint8_t h3lis200dl__who_am_i(void);

void h3lis200dl__dump(void);