add_subdirectory(H3LIS200DL)
add_subdirectory(H3LIS200DL_task)

add_subdirectory(MAX30102)

add_subdirectory(xbee_sender)

add_subdirectory(alarm_task)

add_subdirectory(i2c_wrapper)
