#pragma once

#include <stdbool.h>
#include <stdint.h>

/*
    @brief This wapper only works with i2c0, pin 0(sda) and pin 1(scl) 
    @return true if it is initialized, false if not
*/
bool i2c_wrapper__i2c0_init(void);

bool i2c_wrapper__i2c0_detect(uint8_t slave_address);

bool i2c_wrapper__i2c0_write(uint8_t slave_address, uint8_t register_address, const uint8_t* data, uint8_t length);
bool i2c_wrapper__i2c0_write_single(uint8_t slave_address, uint8_t register_address, uint8_t data);

bool i2c_wrapper__i2c0_read(uint8_t slave_address, uint8_t register_address, uint8_t* data, uint8_t length);

bool i2c_wrapper__bus_scan(void);