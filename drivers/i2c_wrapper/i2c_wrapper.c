#include "i2c_wrapper.h"

#include <stdio.h>

#include "hardware/i2c.h"
#include "hardware/gpio.h"

#include "FreeRTOS.h"
#include "semphr.h"

static const uint32_t i2c0_speed = 250 * 1000;
static const uint8_t sda_pin = 0;
static const uint8_t scl_pin = 1;
static const i2c_inst_t* i2c = i2c0;

static SemaphoreHandle_t mutex;


static bool initialized = false;

bool i2c_wrapper__i2c0_init(void){
    if (!initialized){
        const bool result = i2c_init(i2c, i2c0_speed) == i2c0_speed;

        mutex = xSemaphoreCreateMutex();

        if(NULL != mutex && result){
            gpio_set_function(sda_pin, GPIO_FUNC_I2C);
            gpio_set_function(scl_pin, GPIO_FUNC_I2C);
            gpio_pull_up(sda_pin);
            gpio_pull_up(scl_pin);

            initialized = true;
        }
    }

    return initialized;
}

bool i2c_wrapper__i2c0_write(uint8_t slave_address, uint8_t register_address, const uint8_t* data, uint8_t length){
    const uint8_t buffer_size = length + 1;

    uint8_t buffer[buffer_size];
    buffer[0] = register_address;
    for (uint8_t i = 1; i < buffer_size; i++){
        buffer[i] = data[i-1];
    }
    
    int8_t byte_written = 0;

    if (xSemaphoreTake(mutex, portMAX_DELAY)) {
        byte_written = i2c_write_blocking(i2c, slave_address, buffer, buffer_size, false);
        xSemaphoreGive(mutex);
    }

    return byte_written == (buffer_size - 1);
}

bool i2c_wrapper__i2c0_write_single(uint8_t slave_address, uint8_t register_address, uint8_t data){
    const uint8_t two_byte= 2;

    uint8_t write_buffer[two_byte];
    write_buffer[0] = register_address;
    write_buffer[1] = data;

    int8_t byte_written = 0;

    if (xSemaphoreTake(mutex, portMAX_DELAY)) {
        byte_written = i2c_write_blocking(i2c, slave_address, write_buffer, two_byte, false);
        xSemaphoreGive(mutex);
    }

    return byte_written == two_byte;
}


bool i2c_wrapper__i2c0_read(uint8_t slave_address, uint8_t register_address, uint8_t* data, uint8_t length){
    const uint8_t one_byte = 1;

    int8_t byte_read = 0;
    if (xSemaphoreTake(mutex, portMAX_DELAY)) {
        i2c_write_blocking(i2c0, slave_address, &register_address, one_byte, true);
        byte_read = i2c_read_blocking(i2c, slave_address, data, length, false);
        xSemaphoreGive(mutex);
    }

    return byte_read == length;
}

static bool reserved_addr(uint8_t addr) {
    return (addr & 0x78) == 0 || (addr & 0x78) == 0x78;
}

bool i2c_wrapper__bus_scan(void){
    printf("\nI2C Bus Scan\n");
    printf("   0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F\n");

    for (int addr = 0; addr < (1 << 7); ++addr) {
        if (addr % 16 == 0) {
            printf("%02x ", addr);
        }

        // Perform a 1-byte dummy read from the probe address. If a slave
        // acknowledges this address, the function returns the number of bytes
        // transferred. If the address byte is ignored, the function returns
        // -1.

        // Skip over any reserved addresses.
        int ret;
        uint8_t rxdata;
        if (reserved_addr(addr))
            ret = PICO_ERROR_GENERIC;
        else
            ret = i2c_read_blocking(i2c, addr, &rxdata, 1, false);

        printf(ret < 0 ? "." : "@");
        printf(addr % 16 == 15 ? "\n" : "  ");
    }
    printf("Done.\n");
}