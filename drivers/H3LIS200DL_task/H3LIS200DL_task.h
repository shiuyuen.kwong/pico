#pragma once

#include "H3LIS200DL.h"

void h3lis200dl_task_startup(void);
uint8_t h3lis200dl__get_latest_calculated_g(void);
uint8_t h3lis200dl__get_max_calculated_g(void);