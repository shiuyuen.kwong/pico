#include "H3LIS200DL_task.h"
#include "H3LIS200DL.h"

#include "FreeRTOS.h"
#include "task.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

static h3lis200dl_s h3lis200dl_data;
static uint8_t calculated_g;
static uint8_t max_calculated_g;

static void update_max_calculated_g(void){
    if(calculated_g > max_calculated_g){
        max_calculated_g = calculated_g;
    }
}

static void h3lis200dl_task(void *param) {
    const uint32_t delay_1000_ms = 1000;
    memset(&h3lis200dl_data, 0, sizeof(h3lis200dl_data));

    h3lis200dl__init();

    while (1) {
        h3lis200dl__read();
        calculated_g = h3lis200dl__get_data();
        printf("G: %d\n", calculated_g);

        vTaskDelay(delay_1000_ms);
    }

    vTaskSuspend(NULL);
}

void h3lis200dl_task_startup(void){
    xTaskCreate(h3lis200dl_task, "H3LIS200DL", 1000, NULL, 2, NULL);
}


uint8_t h3lis200dl__get_latest_calculated_g(void) { return calculated_g;}

uint8_t h3lis200dl__get_max_calculated_g(void) { return max_calculated_g;}