#include "alarm_task.h"

#include "FreeRTOS.h"
#include "task.h"

#include "H3LIS200DL_task.h"
#include "alarms.h"
#include "spo2_task.h"


typedef enum { SAFE_FORCE, MILD_FORCE, SEVERE_FORCE } max_force_values_e;
typedef enum { LED_GREEN, LED_YELLOW, LED_RED } led_state_e;
typedef enum { ALARM_OFF, ALARM_WARNING, ALARM_HIGH } alarm_state_e;
typedef enum { NORMAL_BPM, ABNORMAL_BPM, EXTREMELY_ABNORMAL_BPM } bpm_state_e;
typedef enum { NORMAL_SPO2, ABNORMAL_SPO2, EXTREMELY_ABNORMAL_SPO2 } spo2_state_e;

const uint8_t SEVERE_GS = 7;
const uint8_t MILD_GS = 4;

static led_state_e led_state = LED_GREEN;
static max_force_values_e concussive_force_endured_by_user = SAFE_FORCE;
static alarm_state_e alarm_state = ALARM_OFF;


static void determine_max_concussive_force_on_user(float max_gs) {
	 switch(concussive_force_endured_by_user){
		case SAFE_FORCE:
			if(max_gs > MILD_GS){
				concussive_force_endured_by_user = MILD_FORCE;
			}
		case MILD_FORCE:
			if(max_gs > SEVERE_GS){
				concussive_force_endured_by_user = SEVERE_FORCE;
			}
		case SEVERE_FORCE:
			break;
	 }
}

static bpm_state_e determine_bpm_state(uint8_t avg_bpm) {
	bpm_state_e bpm_state;

	if (avg_bpm < 50 || avg_bpm > 120) {
		bpm_state = EXTREMELY_ABNORMAL_BPM;
	} else if (avg_bpm < 60 || avg_bpm > 100) {
		bpm_state = ABNORMAL_BPM;
	} else {
		bpm_state = NORMAL_BPM;
	}

	return bpm_state;
}

static spo2_state_e determine_spo2_state(float spo2) {
	spo2_state_e spo2_state;

	if (spo2 <= 85) {
		spo2_state = EXTREMELY_ABNORMAL_SPO2;
	} else if (spo2 <= 90) {
		spo2_state = ABNORMAL_SPO2;
	} else {
		spo2_state = NORMAL_SPO2;
	}

	return spo2_state;
}

static alarm_state_e alarm_task__state_machine_get_next_state(uint8_t max_gs, uint8_t avg_bpm, float spo2) {
	const bpm_state_e bpm_state = determine_bpm_state(avg_bpm);
	const spo2_state_e spo2_state = determine_spo2_state(spo2);
	determine_max_concussive_force_on_user(max_gs);
	
	alarm_state_e alarm_state;
	if (bpm_state == EXTREMELY_ABNORMAL_BPM || concussive_force_endured_by_user == SEVERE_FORCE || spo2_state == EXTREMELY_ABNORMAL_SPO2) {
		alarm_state = ALARM_HIGH;
	} else if (bpm_state == ABNORMAL_BPM || concussive_force_endured_by_user == MILD_FORCE || spo2_state == ABNORMAL_SPO2) {
		alarm_state = ALARM_WARNING;
	} else {
		alarm_state = ALARM_OFF;
	}

	return alarm_state;
}

static void alarm_task__state_machine(uint8_t max_gs, uint8_t avg_bpm, float spo2) {
	alarm_state_e alarm_state = alarm_task__state_machine_get_next_state(max_gs, avg_bpm, spo2);
	switch(alarm_state){
		case ALARM_OFF:
			led_state = LED_GREEN;
			alarms__off();
			break;
		case ALARM_WARNING:
			led_state = LED_YELLOW;
			alarms__warning();
			break;
		case ALARM_HIGH:
			led_state = LED_RED;
			alarms__high();
			break;
	}
}

static void alarm_task(void *p) {
	alarms__setup();
	while (1) {
		const uint8_t avg_bpm = grab_avg_heart_rate();
		const uint8_t max_gs = h3lis200dl__get_max_calculated_g();
		const int spo2 = grab_avg_spo2();

		alarm_task__state_machine(max_gs, avg_bpm, spo2);
		vTaskDelay(1000);
	}
}

bool alarm_task__startup(void) {
	return (bool)xTaskCreate(alarm_task, "alarm", (2048 / sizeof(void *)), NULL, 2, NULL);
}