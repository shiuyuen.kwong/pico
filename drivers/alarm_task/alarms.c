#include "alarms.h"
#include "hardware/pwm.h"
#include "hardware/gpio.h"

const uint8_t PWM_RESOLUTION = 100;
const uint8_t RED_PIN = 6;    
const uint8_t GREEN_PIN = 7;  
const uint8_t BLUE_PIN = 8;   

static void alarms__initialize_pwm_pins(void) {
    gpio_set_function(RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(BLUE_PIN, GPIO_FUNC_PWM);
}

static void alarms__pwm_init(void) {
    pwm_set_wrap(pwm_gpio_to_slice_num(RED_PIN), PWM_RESOLUTION);
    pwm_set_enabled(pwm_gpio_to_slice_num(RED_PIN), true);

    pwm_set_wrap(pwm_gpio_to_slice_num(GREEN_PIN), PWM_RESOLUTION);
    pwm_set_enabled(pwm_gpio_to_slice_num(GREEN_PIN), true);
    
    pwm_set_wrap(pwm_gpio_to_slice_num(BLUE_PIN), PWM_RESOLUTION);
    pwm_set_enabled(pwm_gpio_to_slice_num(BLUE_PIN), true);
}

void alarms__setup(void) {
    alarms__initialize_pwm_pins();
    alarms__pwm_init();
    alarms__off();
}

void alarms__warning(void) {
    pwm_set_gpio_level(RED_PIN, 0);
    pwm_set_gpio_level(GREEN_PIN, 80);
    pwm_set_gpio_level(BLUE_PIN, 100);
}

void alarms__high(void) {
    pwm_set_gpio_level(RED_PIN, 0);
    pwm_set_gpio_level(GREEN_PIN, 100);
    pwm_set_gpio_level(BLUE_PIN, 100);
}

void alarms__off(void) {
    pwm_set_gpio_level(RED_PIN, 100);
    pwm_set_gpio_level(GREEN_PIN, 0);
    pwm_set_gpio_level(BLUE_PIN, 100);
}
