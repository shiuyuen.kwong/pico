#pragma once

void alarms__setup(void);
void alarms__warning(void);
void alarms__high(void);
void alarms__off(void);