SDK setup
```
sudo apt update  
sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential   

git submodule update --init --recursive

cd build
cmake .. 

sudo adduser <your_user_name> dialout
```

To compile
```
make -C ./build/
```

`sudo chmod a+rw /dev/ttyACM0` might be needed 